package com.rantion.campingword_crawler.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReviewDTO {
    /**
     * 评论对应商品id
     */
    private String productId;
    /**
     * 评论内容
     */
    private String reviewText;
    /**
     * 用户给商品星级
     */
    private String rating;
    /**
     * 评论提交时间
     */
    private Date submissionTime;
    /**
     * 评论用户昵称
     */
    private String userNickName;
    /**
     * 原始json数据
     */
    private String originalData;
}
package com.rantion.campingword_crawler.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.TreeMap;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {
    private String id;
    private String category;
    private String name;
    private Double discountPrice;
    /**
     * 显示的评论数量
     */
    private Integer showReviewCounts;
    /**
     * 真正评论数量
     */
    private Integer realReviewCounts;
    /**
     * 评论列表
     */
    private List<ReviewDTO> reviewList;
    /**
     * 保存原始json数据
     */
    private String originalData;
    /*
    private TreeMap<String,Integer> frequentlyWords;
     */
}
